package net.omatic.torrent.parser.admin.entity;

import javax.persistence.*;

@Entity
@Table(name = "task_has_notification_email", schema = "", catalog = "torrent_parser")
public class TaskHasNotificationEmailEntity {
    private int taskHasNotificationEmailId;
    private int taskHasNotificationEmailTaskId;
    private int taskHasNotificationEmailNotificationEmailId;
    private TaskEntity taskByTaskHasNotificationEmailTaskEntityId;
    private NotificationEmailEntity notificationEmailByTaskHasNotificationEmailNotificationEmailEntityId;

    @Id
    @Column(name = "task_has_notification_email_id", nullable = false, insertable = true, updatable = true)
    public int getTaskHasNotificationEmailId() {
        return taskHasNotificationEmailId;
    }

    public void setTaskHasNotificationEmailId(int taskHasNotificationEmailId) {
        this.taskHasNotificationEmailId = taskHasNotificationEmailId;
    }

    @Basic
    @Column(name = "task_has_notification_email_task_id", nullable = false, insertable = false, updatable = false)
    public int getTaskHasNotificationEmailTaskId() {
        return taskHasNotificationEmailTaskId;
    }

    public void setTaskHasNotificationEmailTaskId(int taskHasNotificationEmailTaskId) {
        this.taskHasNotificationEmailTaskId = taskHasNotificationEmailTaskId;
    }

    @Basic
    @Column(name = "task_has_notification_email_notification_email_id", nullable = false, insertable = false, updatable = false)
    public int getTaskHasNotificationEmailNotificationEmailId() {
        return taskHasNotificationEmailNotificationEmailId;
    }

    public void setTaskHasNotificationEmailNotificationEmailId(int taskHasNotificationEmailNotificationEmailId) {
        this.taskHasNotificationEmailNotificationEmailId = taskHasNotificationEmailNotificationEmailId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskHasNotificationEmailEntity that = (TaskHasNotificationEmailEntity) o;

        if (taskHasNotificationEmailId != that.taskHasNotificationEmailId) return false;
        if (taskHasNotificationEmailTaskId != that.taskHasNotificationEmailTaskId) return false;
        if (taskHasNotificationEmailNotificationEmailId != that.taskHasNotificationEmailNotificationEmailId)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = taskHasNotificationEmailId;
        result = 31 * result + taskHasNotificationEmailTaskId;
        result = 31 * result + taskHasNotificationEmailNotificationEmailId;
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "task_has_notification_email_task_id", referencedColumnName = "task_id", nullable = false)
    public TaskEntity getTaskByTaskHasNotificationEmailTaskEntityId() {
        return taskByTaskHasNotificationEmailTaskEntityId;
    }

    public void setTaskByTaskHasNotificationEmailTaskEntityId(TaskEntity taskByTaskHasNotificationEmailTaskEntityId) {
        this.taskByTaskHasNotificationEmailTaskEntityId = taskByTaskHasNotificationEmailTaskEntityId;
    }

    @ManyToOne
    @JoinColumn(name = "task_has_notification_email_notification_email_id", referencedColumnName = "notification_email_id", nullable = false)
    public NotificationEmailEntity getNotificationEmailByTaskHasNotificationEmailNotificationEmailEntityId() {
        return notificationEmailByTaskHasNotificationEmailNotificationEmailEntityId;
    }

    public void setNotificationEmailByTaskHasNotificationEmailNotificationEmailEntityId(NotificationEmailEntity notificationEmailByTaskHasNotificationEmailNotificationEmailEntityId) {
        this.notificationEmailByTaskHasNotificationEmailNotificationEmailEntityId = notificationEmailByTaskHasNotificationEmailNotificationEmailEntityId;
    }
}
