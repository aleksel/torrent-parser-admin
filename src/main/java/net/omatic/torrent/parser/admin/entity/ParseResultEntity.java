package net.omatic.torrent.parser.admin.entity;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "parse_result", schema = "", catalog = "torrent_parser")
public class ParseResultEntity {
    private int parseResultId;
    private int parseResultTaskId;
    private int parseResultPageChanged;
    private String parseResultRawPage;
    private String parseResultSearchedElement;
    private Timestamp parseResultCreateTime;
    private TaskEntity taskByParseResultTaskEntityId;

    @Id
    @Column(name = "parse_result_id", nullable = false, insertable = true, updatable = true)
    public int getParseResultId() {
        return parseResultId;
    }

    public void setParseResultId(int parseResultId) {
        this.parseResultId = parseResultId;
    }

    @Basic
    @Column(name = "parse_result_task_id", nullable = false, insertable = false, updatable = false)
    public int getParseResultTaskId() {
        return parseResultTaskId;
    }

    public void setParseResultTaskId(int parseResultTaskId) {
        this.parseResultTaskId = parseResultTaskId;
    }

    @Basic
    @Column(name = "parse_result_page_changed", nullable = false, insertable = true, updatable = true)
    public int getParseResultPageChanged() {
        return parseResultPageChanged;
    }

    public void setParseResultPageChanged(int parseResultPageChanged) {
        this.parseResultPageChanged = parseResultPageChanged;
    }

    @Basic
    @Column(name = "parse_result_raw_page", nullable = false, insertable = true, updatable = true, length = 2147483647)
    public String getParseResultRawPage() {
        return parseResultRawPage;
    }

    public void setParseResultRawPage(String parseResultRawPage) {
        this.parseResultRawPage = parseResultRawPage;
    }

    @Basic
    @Column(name = "parse_result_searched_element", nullable = false, insertable = true, updatable = true, length = 255)
    public String getParseResultSearchedElement() {
        return parseResultSearchedElement;
    }

    public void setParseResultSearchedElement(String parseResultSearchedElement) {
        this.parseResultSearchedElement = parseResultSearchedElement;
    }

    @Basic
    @Column(name = "parse_result_create_time", nullable = false, insertable = true, updatable = true)
    public Timestamp getParseResultCreateTime() {
        return parseResultCreateTime;
    }

    public void setParseResultCreateTime(Timestamp parseResultCreateTime) {
        this.parseResultCreateTime = parseResultCreateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParseResultEntity that = (ParseResultEntity) o;

        if (parseResultId != that.parseResultId) return false;
        if (parseResultTaskId != that.parseResultTaskId) return false;
        if (parseResultPageChanged != that.parseResultPageChanged) return false;
        if (parseResultRawPage != null ? !parseResultRawPage.equals(that.parseResultRawPage) : that.parseResultRawPage != null)
            return false;
        if (parseResultSearchedElement != null ? !parseResultSearchedElement.equals(that.parseResultSearchedElement) : that.parseResultSearchedElement != null)
            return false;
        if (parseResultCreateTime != null ? !parseResultCreateTime.equals(that.parseResultCreateTime) : that.parseResultCreateTime != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = parseResultId;
        result = 31 * result + parseResultTaskId;
        result = 31 * result + parseResultPageChanged;
        result = 31 * result + (parseResultRawPage != null ? parseResultRawPage.hashCode() : 0);
        result = 31 * result + (parseResultSearchedElement != null ? parseResultSearchedElement.hashCode() : 0);
        result = 31 * result + (parseResultCreateTime != null ? parseResultCreateTime.hashCode() : 0);
        return result;
    }

    /*@ManyToOne
    @JoinColumn(name = "parse_result_task_id", referencedColumnName = "task_id", nullable = false)
    public TaskEntity getTaskByParseResultTaskEntityId() {
        return taskByParseResultTaskEntityId;
    }*/

    /*public void setTaskByParseResultTaskEntityId(TaskEntity taskByParseResultTaskEntityId) {
        this.taskByParseResultTaskEntityId = taskByParseResultTaskEntityId;
    }*/
}
