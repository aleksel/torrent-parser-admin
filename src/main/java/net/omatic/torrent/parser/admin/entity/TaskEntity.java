package net.omatic.torrent.parser.admin.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Collection;

@Entity(name = "task")
public class TaskEntity {
    private int taskId;
    private Integer taskAuthDataId;
    private String taskUrl;
    private String taskName;
    private String taskSearchTag;
    private String taskComparisonElement;
    private int taskActive;
    private Timestamp taskCreateTime;
    private Timestamp taskUpdateTime;
    private Collection<ParseResultEntity> parseResultId;
    private AuthDataEntity authDataByTaskAuthDataEntityId;
    private Collection<TaskHasNotificationEmailEntity> taskHasNotificationEmailsByTaskIdEntity;
    private Collection<TaskHasTaskCategoryEntity> taskHasTaskCategoriesByTaskId;

    @Id
    @Column(name = "task_id", nullable = false, insertable = true, updatable = true)
    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    @Basic
    @Column(name = "task_auth_data_id", nullable = true, insertable = true, updatable = true)
    public Integer getTaskAuthDataId() {
        return taskAuthDataId;
    }

    public void setTaskAuthDataId(Integer taskAuthDataId) {
        this.taskAuthDataId = taskAuthDataId;
    }

    @Basic
    @Column(name = "task_url", nullable = false, insertable = true, updatable = true, length = 255)
    public String getTaskUrl() {
        return taskUrl;
    }

    public void setTaskUrl(String taskUrl) {
        this.taskUrl = taskUrl;
    }

    @Basic
    @Column(name = "task_name", nullable = false, insertable = true, updatable = true, length = 255)
    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    @Basic
    @Column(name = "task_search_tag", nullable = false, insertable = true, updatable = true, length = 255)
    public String getTaskSearchTag() {
        return taskSearchTag;
    }

    public void setTaskSearchTag(String taskSearchTag) {
        this.taskSearchTag = taskSearchTag;
    }

    @Basic
    @Column(name = "task_comparison_element", nullable = false, insertable = true, updatable = true, length = 65535)
    public String getTaskComparisonElement() {
        return taskComparisonElement;
    }

    public void setTaskComparisonElement(String taskComparisonElement) {
        this.taskComparisonElement = taskComparisonElement;
    }

    @Basic
    @Column(name = "task_active", nullable = false, insertable = true, updatable = true)
    public int getTaskActive() {
        return taskActive;
    }

    public void setTaskActive(int taskActive) {
        this.taskActive = taskActive;
    }

    @Basic
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm a z")
    @Column(name = "task_create_time", nullable = false, insertable = true, updatable = true)
    public Timestamp getTaskCreateTime() {
        return taskCreateTime;
    }

    public void setTaskCreateTime(Timestamp taskCreateTime) {
        this.taskCreateTime = taskCreateTime;
    }

    @Basic
    @Column(name = "task_update_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getTaskUpdateTime() {
        return taskUpdateTime;
    }

    public void setTaskUpdateTime(Timestamp taskUpdateTime) {
        this.taskUpdateTime = taskUpdateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskEntity taskEntity = (TaskEntity) o;

        if (taskId != taskEntity.taskId) return false;
        if (taskActive != taskEntity.taskActive) return false;
        if (taskAuthDataId != null ? !taskAuthDataId.equals(taskEntity.taskAuthDataId) : taskEntity.taskAuthDataId != null)
            return false;
        if (taskUrl != null ? !taskUrl.equals(taskEntity.taskUrl) : taskEntity.taskUrl != null) return false;
        if (taskName != null ? !taskName.equals(taskEntity.taskName) : taskEntity.taskName != null) return false;
        if (taskSearchTag != null ? !taskSearchTag.equals(taskEntity.taskSearchTag) : taskEntity.taskSearchTag != null)
            return false;
        if (taskComparisonElement != null ? !taskComparisonElement.equals(taskEntity.taskComparisonElement) : taskEntity.taskComparisonElement != null)
            return false;
        if (taskCreateTime != null ? !taskCreateTime.equals(taskEntity.taskCreateTime) : taskEntity.taskCreateTime != null)
            return false;
        if (taskUpdateTime != null ? !taskUpdateTime.equals(taskEntity.taskUpdateTime) : taskEntity.taskUpdateTime != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = taskId;
        result = 31 * result + (taskAuthDataId != null ? taskAuthDataId.hashCode() : 0);
        result = 31 * result + (taskUrl != null ? taskUrl.hashCode() : 0);
        result = 31 * result + (taskName != null ? taskName.hashCode() : 0);
        result = 31 * result + (taskSearchTag != null ? taskSearchTag.hashCode() : 0);
        result = 31 * result + (taskComparisonElement != null ? taskComparisonElement.hashCode() : 0);
        result = 31 * result + taskActive;
        result = 31 * result + (taskCreateTime != null ? taskCreateTime.hashCode() : 0);
        result = 31 * result + (taskUpdateTime != null ? taskUpdateTime.hashCode() : 0);
        return result;
    }

   /* @OneToMany(mappedBy = "parseResultResultId")
    public Collection<ParseResultEntity> getParseResultId() {
        return parseResultId;
    }

    public void setParseResultId(Collection<ParseResultEntity> parseResultId) {
        this.parseResultId = parseResultId;
    }*/

    /*@OneToMany(mappedBy = "getTaskByTaskHasNotificationEmailTaskEntityId")
    public Collection<TaskHasNotificationEmailEntity> getTaskHasNotificationEmailsByTaskIdEntity() {
        return taskHasNotificationEmailsByTaskIdEntity;
    }

    public void setTaskHasNotificationEmailsByTaskIdEntity(Collection<TaskHasNotificationEmailEntity> taskHasNotificationEmailsByTaskIdEntity) {
        this.taskHasNotificationEmailsByTaskIdEntity = taskHasNotificationEmailsByTaskIdEntity;
    }

    @OneToMany(mappedBy = "getTaskByTaskHasTaskCategoryTaskEntityId")
    public Collection<TaskHasTaskCategoryEntity> getTaskHasTaskCategoriesByTaskId() {
        return taskHasTaskCategoriesByTaskId;
    }

    public void setTaskHasTaskCategoriesByTaskId(Collection<TaskHasTaskCategoryEntity> taskHasTaskCategoriesByTaskId) {
        this.taskHasTaskCategoriesByTaskId = taskHasTaskCategoriesByTaskId;
    }*/
}
