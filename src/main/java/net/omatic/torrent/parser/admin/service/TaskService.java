package net.omatic.torrent.parser.admin.service;

import net.omatic.torrent.parser.admin.dao.interfaces.TaskInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Component
@Path("/get-task")
@Produces(MediaType.APPLICATION_JSON)
public class TaskService {
    private static final Logger logger = LoggerFactory.getLogger(TaskService.class);
    @Autowired
    private TaskInterface tasks;

    @GET
    public List getAll()
    {
        logger.debug("TTTTTTTTTEEEEEEESTTTTTTTTTT2");
        logger.info("TTTTTTTTTEEEEEEESTTTTTTTTTT3");
        logger.error("TTTTTTTTTEEEEEEESTTTTTTTTTT4");
        return tasks.getAll();
    }
}
