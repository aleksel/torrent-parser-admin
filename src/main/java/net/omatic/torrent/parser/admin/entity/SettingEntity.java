package net.omatic.torrent.parser.admin.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class SettingEntity {
    private int settingId;
    private String settingName;
    private String settingSystemName;
    private String settingValue;
    private String settingType;
    private Timestamp settingCreateTime;
    private Timestamp settingUpdateTime;

    @Id
    @Column(name = "setting_id", nullable = false, insertable = true, updatable = true)
    public int getSettingId() {
        return settingId;
    }

    public void setSettingId(int settingId) {
        this.settingId = settingId;
    }

    @Basic
    @Column(name = "setting_name", nullable = true, insertable = true, updatable = true, length = 255)
    public String getSettingName() {
        return settingName;
    }

    public void setSettingName(String settingName) {
        this.settingName = settingName;
    }

    @Basic
    @Column(name = "setting_system_name", nullable = false, insertable = true, updatable = true, length = 255)
    public String getSettingSystemName() {
        return settingSystemName;
    }

    public void setSettingSystemName(String settingSystemName) {
        this.settingSystemName = settingSystemName;
    }

    @Basic
    @Column(name = "setting_value", nullable = false, insertable = true, updatable = true, length = 65535)
    public String getSettingValue() {
        return settingValue;
    }

    public void setSettingValue(String settingValue) {
        this.settingValue = settingValue;
    }

    @Basic
    @Column(name = "setting_type", nullable = false, insertable = true, updatable = true, length = 9)
    public String getSettingType() {
        return settingType;
    }

    public void setSettingType(String settingType) {
        this.settingType = settingType;
    }

    @Basic
    @Column(name = "setting_create_time", nullable = false, insertable = true, updatable = true)
    public Timestamp getSettingCreateTime() {
        return settingCreateTime;
    }

    public void setSettingCreateTime(Timestamp settingCreateTime) {
        this.settingCreateTime = settingCreateTime;
    }

    @Basic
    @Column(name = "setting_update_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getSettingUpdateTime() {
        return settingUpdateTime;
    }

    public void setSettingUpdateTime(Timestamp settingUpdateTime) {
        this.settingUpdateTime = settingUpdateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SettingEntity settingEntity = (SettingEntity) o;

        if (settingId != settingEntity.settingId) return false;
        if (settingName != null ? !settingName.equals(settingEntity.settingName) : settingEntity.settingName != null) return false;
        if (settingSystemName != null ? !settingSystemName.equals(settingEntity.settingSystemName) : settingEntity.settingSystemName != null)
            return false;
        if (settingValue != null ? !settingValue.equals(settingEntity.settingValue) : settingEntity.settingValue != null)
            return false;
        if (settingType != null ? !settingType.equals(settingEntity.settingType) : settingEntity.settingType != null) return false;
        if (settingCreateTime != null ? !settingCreateTime.equals(settingEntity.settingCreateTime) : settingEntity.settingCreateTime != null)
            return false;
        if (settingUpdateTime != null ? !settingUpdateTime.equals(settingEntity.settingUpdateTime) : settingEntity.settingUpdateTime != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = settingId;
        result = 31 * result + (settingName != null ? settingName.hashCode() : 0);
        result = 31 * result + (settingSystemName != null ? settingSystemName.hashCode() : 0);
        result = 31 * result + (settingValue != null ? settingValue.hashCode() : 0);
        result = 31 * result + (settingType != null ? settingType.hashCode() : 0);
        result = 31 * result + (settingCreateTime != null ? settingCreateTime.hashCode() : 0);
        result = 31 * result + (settingUpdateTime != null ? settingUpdateTime.hashCode() : 0);
        return result;
    }
}
