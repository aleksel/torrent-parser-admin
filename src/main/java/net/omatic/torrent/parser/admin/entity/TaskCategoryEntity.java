package net.omatic.torrent.parser.admin.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "task_category", schema = "", catalog = "torrent_parser")
public class TaskCategoryEntity {
    private int taskCategoryId;
    private String taskCategoryName;
    private int taskCategoryActive;
    private Timestamp taskCategoryCreateTime;
    private Timestamp taskCategoryUpdateTime;
    private Collection<TaskHasTaskCategoryEntity> taskHasTaskCategoriesByTaskCategoryId;

    @Id
    @Column(name = "task_category_id", nullable = false, insertable = true, updatable = true)
    public int getTaskCategoryId() {
        return taskCategoryId;
    }

    public void setTaskCategoryId(int taskCategoryId) {
        this.taskCategoryId = taskCategoryId;
    }

    @Basic
    @Column(name = "task_category_name", nullable = false, insertable = true, updatable = true, length = 255)
    public String getTaskCategoryName() {
        return taskCategoryName;
    }

    public void setTaskCategoryName(String taskCategoryName) {
        this.taskCategoryName = taskCategoryName;
    }

    @Basic
    @Column(name = "task_category_active", nullable = false, insertable = true, updatable = true)
    public int getTaskCategoryActive() {
        return taskCategoryActive;
    }

    public void setTaskCategoryActive(int taskCategoryActive) {
        this.taskCategoryActive = taskCategoryActive;
    }

    @Basic
    @Column(name = "task_category_create_time", nullable = false, insertable = true, updatable = true)
    public Timestamp getTaskCategoryCreateTime() {
        return taskCategoryCreateTime;
    }

    public void setTaskCategoryCreateTime(Timestamp taskCategoryCreateTime) {
        this.taskCategoryCreateTime = taskCategoryCreateTime;
    }

    @Basic
    @Column(name = "task_category_update_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getTaskCategoryUpdateTime() {
        return taskCategoryUpdateTime;
    }

    public void setTaskCategoryUpdateTime(Timestamp taskCategoryUpdateTime) {
        this.taskCategoryUpdateTime = taskCategoryUpdateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskCategoryEntity that = (TaskCategoryEntity) o;

        if (taskCategoryId != that.taskCategoryId) return false;
        if (taskCategoryActive != that.taskCategoryActive) return false;
        if (taskCategoryName != null ? !taskCategoryName.equals(that.taskCategoryName) : that.taskCategoryName != null)
            return false;
        if (taskCategoryCreateTime != null ? !taskCategoryCreateTime.equals(that.taskCategoryCreateTime) : that.taskCategoryCreateTime != null)
            return false;
        if (taskCategoryUpdateTime != null ? !taskCategoryUpdateTime.equals(that.taskCategoryUpdateTime) : that.taskCategoryUpdateTime != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = taskCategoryId;
        result = 31 * result + (taskCategoryName != null ? taskCategoryName.hashCode() : 0);
        result = 31 * result + taskCategoryActive;
        result = 31 * result + (taskCategoryCreateTime != null ? taskCategoryCreateTime.hashCode() : 0);
        result = 31 * result + (taskCategoryUpdateTime != null ? taskCategoryUpdateTime.hashCode() : 0);
        return result;
    }

    /*@OneToMany(mappedBy = "getTaskCategoryByTaskHasTaskCategoryTaskCategoryEntityId")
    public Collection<TaskHasTaskCategoryEntity> getTaskHasTaskCategoriesByTaskCategoryId() {
        return taskHasTaskCategoriesByTaskCategoryId;
    }

    public void setTaskHasTaskCategoriesByTaskCategoryId(Collection<TaskHasTaskCategoryEntity> taskHasTaskCategoriesByTaskCategoryId) {
        this.taskHasTaskCategoriesByTaskCategoryId = taskHasTaskCategoriesByTaskCategoryId;
    }*/
}
