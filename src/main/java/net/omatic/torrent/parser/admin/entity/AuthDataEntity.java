package net.omatic.torrent.parser.admin.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "auth_data", schema = "", catalog = "torrent_parser")
public class AuthDataEntity {
    private int authDataId;
    private String authDataName;
    private String authDataUrl;
    private String authDataData;
    private Timestamp authDataCreateTime;
    private Timestamp authDataUpdateTime;
    private Collection<TaskEntity> tasksByAuthDataId;

    @Id
    @Column(name = "auth_data_id", nullable = false, insertable = true, updatable = true)
    public int getAuthDataId() {
        return authDataId;
    }

    public void setAuthDataId(int authDataId) {
        this.authDataId = authDataId;
    }

    @Basic
    @Column(name = "auth_data_name", nullable = false, insertable = true, updatable = true, length = 255)
    public String getAuthDataName() {
        return authDataName;
    }

    public void setAuthDataName(String authDataName) {
        this.authDataName = authDataName;
    }

    @Basic
    @Column(name = "auth_data_url", nullable = false, insertable = true, updatable = true, length = 255)
    public String getAuthDataUrl() {
        return authDataUrl;
    }

    public void setAuthDataUrl(String authDataUrl) {
        this.authDataUrl = authDataUrl;
    }

    @Basic
    @Column(name = "auth_data_data", nullable = true, insertable = true, updatable = true, length = 255)
    public String getAuthDataData() {
        return authDataData;
    }

    public void setAuthDataData(String authDataData) {
        this.authDataData = authDataData;
    }

    @Basic
    @Column(name = "auth_data_create_time", nullable = false, insertable = true, updatable = true)
    public Timestamp getAuthDataCreateTime() {
        return authDataCreateTime;
    }

    public void setAuthDataCreateTime(Timestamp authDataCreateTime) {
        this.authDataCreateTime = authDataCreateTime;
    }

    @Basic
    @Column(name = "auth_data_update_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getAuthDataUpdateTime() {
        return authDataUpdateTime;
    }

    public void setAuthDataUpdateTime(Timestamp authDataUpdateTime) {
        this.authDataUpdateTime = authDataUpdateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthDataEntity authDataEntity = (AuthDataEntity) o;

        if (authDataId != authDataEntity.authDataId) return false;
        if (authDataName != null ? !authDataName.equals(authDataEntity.authDataName) : authDataEntity.authDataName != null)
            return false;
        if (authDataUrl != null ? !authDataUrl.equals(authDataEntity.authDataUrl) : authDataEntity.authDataUrl != null)
            return false;
        if (authDataData != null ? !authDataData.equals(authDataEntity.authDataData) : authDataEntity.authDataData != null)
            return false;
        if (authDataCreateTime != null ? !authDataCreateTime.equals(authDataEntity.authDataCreateTime) : authDataEntity.authDataCreateTime != null)
            return false;
        if (authDataUpdateTime != null ? !authDataUpdateTime.equals(authDataEntity.authDataUpdateTime) : authDataEntity.authDataUpdateTime != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = authDataId;
        result = 31 * result + (authDataName != null ? authDataName.hashCode() : 0);
        result = 31 * result + (authDataUrl != null ? authDataUrl.hashCode() : 0);
        result = 31 * result + (authDataData != null ? authDataData.hashCode() : 0);
        result = 31 * result + (authDataCreateTime != null ? authDataCreateTime.hashCode() : 0);
        result = 31 * result + (authDataUpdateTime != null ? authDataUpdateTime.hashCode() : 0);
        return result;
    }

    /*@OneToMany(mappedBy = "getAuthDataByTaskAuthDataEntityId")
    public Collection<TaskEntity> getTasksByAuthDataId() {
        return tasksByAuthDataId;
    }

    public void setTasksByAuthDataId(Collection<TaskEntity> tasksByAuthDataId) {
        this.tasksByAuthDataId = tasksByAuthDataId;
    }*/
}
