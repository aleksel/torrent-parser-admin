package net.omatic.torrent.parser.admin.dao.interfaces;

        import net.omatic.torrent.parser.admin.entity.TaskEntity;

        import java.util.List;

public interface TaskInterface {
    List getAll();
    void add(TaskEntity taskEntity);
    void update(TaskEntity taskEntity);
    void delete(Long id);
}
