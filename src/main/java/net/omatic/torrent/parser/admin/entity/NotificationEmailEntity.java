package net.omatic.torrent.parser.admin.entity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "notification_email", schema = "", catalog = "torrent_parser")
public class NotificationEmailEntity {
    private int notificationEmailId;
    private String notificationEmailEmail;
    private Timestamp notificationEmailCreateTime;
    private Timestamp notificationEmailUpdateTime;
    private Collection<TaskHasNotificationEmailEntity> taskHasNotificationEmailsByNotificationEmailEntityId;

    @Id
    @Column(name = "notification_email_id", nullable = false, insertable = true, updatable = true)
    public int getNotificationEmailId() {
        return notificationEmailId;
    }

    public void setNotificationEmailId(int notificationEmailId) {
        this.notificationEmailId = notificationEmailId;
    }

    @Basic
    @Column(name = "notification_email_email", nullable = false, insertable = true, updatable = true, length = 255)
    public String getNotificationEmailEmail() {
        return notificationEmailEmail;
    }

    public void setNotificationEmailEmail(String notificationEmailEmail) {
        this.notificationEmailEmail = notificationEmailEmail;
    }

    @Basic
    @Column(name = "notification_email_create_time", nullable = false, insertable = true, updatable = true)
    public Timestamp getNotificationEmailCreateTime() {
        return notificationEmailCreateTime;
    }

    public void setNotificationEmailCreateTime(Timestamp notificationEmailCreateTime) {
        this.notificationEmailCreateTime = notificationEmailCreateTime;
    }

    @Basic
    @Column(name = "notification_email_update_time", nullable = true, insertable = true, updatable = true)
    public Timestamp getNotificationEmailUpdateTime() {
        return notificationEmailUpdateTime;
    }

    public void setNotificationEmailUpdateTime(Timestamp notificationEmailUpdateTime) {
        this.notificationEmailUpdateTime = notificationEmailUpdateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NotificationEmailEntity that = (NotificationEmailEntity) o;

        if (notificationEmailId != that.notificationEmailId) return false;
        if (notificationEmailEmail != null ? !notificationEmailEmail.equals(that.notificationEmailEmail) : that.notificationEmailEmail != null)
            return false;
        if (notificationEmailCreateTime != null ? !notificationEmailCreateTime.equals(that.notificationEmailCreateTime) : that.notificationEmailCreateTime != null)
            return false;
        if (notificationEmailUpdateTime != null ? !notificationEmailUpdateTime.equals(that.notificationEmailUpdateTime) : that.notificationEmailUpdateTime != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = notificationEmailId;
        result = 31 * result + (notificationEmailEmail != null ? notificationEmailEmail.hashCode() : 0);
        result = 31 * result + (notificationEmailCreateTime != null ? notificationEmailCreateTime.hashCode() : 0);
        result = 31 * result + (notificationEmailUpdateTime != null ? notificationEmailUpdateTime.hashCode() : 0);
        return result;
    }

    /*@OneToMany(mappedBy = "getNotificationEmailByTaskHasNotificationEmailNotificationEmailEntityId")
    public Collection<TaskHasNotificationEmailEntity> getTaskHasNotificationEmailsByNotificationEmailEntityId() {
        return taskHasNotificationEmailsByNotificationEmailEntityId;
    }

    public void setTaskHasNotificationEmailsByNotificationEmailEntityId(Collection<TaskHasNotificationEmailEntity> taskHasNotificationEmailsByNotificationEmailEntityId) {
        this.taskHasNotificationEmailsByNotificationEmailEntityId = taskHasNotificationEmailsByNotificationEmailEntityId;
    }*/
}
