package net.omatic.torrent.parser.admin.entity;

import javax.persistence.*;

@Entity
@Table(name = "task_has_task_category", schema = "", catalog = "torrent_parser")
public class TaskHasTaskCategoryEntity {
    private int taskHasTaskCategoryId;
    private int taskHasTaskCategoryTaskId;
    private int taskHasTaskCategoryTaskCategoryId;
    private TaskCategoryEntity taskCategoryByTaskHasTaskCategoryTaskCategoryEntityId;
    private TaskEntity taskByTaskHasTaskCategoryTaskEntityId;

    @Id
    @Column(name = "task_has_task_category_id", nullable = false, insertable = true, updatable = true)
    public int getTaskHasTaskCategoryId() {
        return taskHasTaskCategoryId;
    }

    public void setTaskHasTaskCategoryId(int taskHasTaskCategoryId) {
        this.taskHasTaskCategoryId = taskHasTaskCategoryId;
    }

    @Basic
    @Column(name = "task_has_task_category_task_id", nullable = false, insertable = false, updatable = false)
    public int getTaskHasTaskCategoryTaskId() {
        return taskHasTaskCategoryTaskId;
    }

    public void setTaskHasTaskCategoryTaskId(int taskHasTaskCategoryTaskId) {
        this.taskHasTaskCategoryTaskId = taskHasTaskCategoryTaskId;
    }

    @Basic
    @Column(name = "task_has_task_category_task_category_id", nullable = false, insertable = false, updatable = false)
    public int getTaskHasTaskCategoryTaskCategoryId() {
        return taskHasTaskCategoryTaskCategoryId;
    }

    public void setTaskHasTaskCategoryTaskCategoryId(int taskHasTaskCategoryTaskCategoryId) {
        this.taskHasTaskCategoryTaskCategoryId = taskHasTaskCategoryTaskCategoryId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TaskHasTaskCategoryEntity that = (TaskHasTaskCategoryEntity) o;

        if (taskHasTaskCategoryId != that.taskHasTaskCategoryId) return false;
        if (taskHasTaskCategoryTaskId != that.taskHasTaskCategoryTaskId) return false;
        if (taskHasTaskCategoryTaskCategoryId != that.taskHasTaskCategoryTaskCategoryId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = taskHasTaskCategoryId;
        result = 31 * result + taskHasTaskCategoryTaskId;
        result = 31 * result + taskHasTaskCategoryTaskCategoryId;
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "task_has_task_category_task_category_id", referencedColumnName = "task_category_id", nullable = false)
    public TaskCategoryEntity getTaskCategoryByTaskHasTaskCategoryTaskCategoryEntityId() {
        return taskCategoryByTaskHasTaskCategoryTaskCategoryEntityId;
    }

    public void setTaskCategoryByTaskHasTaskCategoryTaskCategoryEntityId(TaskCategoryEntity taskCategoryByTaskHasTaskCategoryTaskCategoryEntityId) {
        this.taskCategoryByTaskHasTaskCategoryTaskCategoryEntityId = taskCategoryByTaskHasTaskCategoryTaskCategoryEntityId;
    }

    @ManyToOne
    @JoinColumn(name = "task_has_task_category_task_id", referencedColumnName = "task_id", nullable = false)
    public TaskEntity getTaskByTaskHasTaskCategoryTaskEntityId() {
        return taskByTaskHasTaskCategoryTaskEntityId;
    }

    public void setTaskByTaskHasTaskCategoryTaskEntityId(TaskEntity taskByTaskHasTaskCategoryTaskEntityId) {
        this.taskByTaskHasTaskCategoryTaskEntityId = taskByTaskHasTaskCategoryTaskEntityId;
    }
}
