package net.omatic.torrent.parser.admin.dao.impl;

import net.omatic.torrent.parser.admin.dao.interfaces.TaskInterface;
import net.omatic.torrent.parser.admin.entity.TaskEntity;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Component
public class TaskDao implements TaskInterface {

    @Autowired
    private SessionFactory sessionFactory;

    private ProjectionList taskProjection;

    public TaskDao() {
        taskProjection = Projections.projectionList();
        taskProjection.add(Projections.property("task_id"), "task_id");
        taskProjection.add(Projections.property("task_name"), "task_name");
        taskProjection.add(Projections.property("task_url"), "task_url");
        taskProjection.add(Projections.property("task_create_time"), "task_create_time");
    }

    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    public List getAll() {
        DetachedCriteria taskListCriteria = DetachedCriteria.forClass(TaskEntity.class, "t");
        Criteria criteria = taskListCriteria.getExecutableCriteria(sessionFactory.getCurrentSession());
        criteria.addOrder(Order.desc("t.taskUpdateTime"));
        criteria.addOrder(Order.desc("t.taskCreateTime"));
                //.setProjection(bookProjection)
                //.setResultTransformer(Transformers.aliasToBean(Book.class));
        return criteria.list();
    }

    public void add(TaskEntity taskEntity) {

    }

    public void update(TaskEntity taskEntity) {

    }

    public void delete(Long id) {

    }
}
